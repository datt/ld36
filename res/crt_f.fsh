#version 140

in vec4 color;
in vec2 tex;

out vec4 fragColor;

uniform sampler2D fbotex;

uniform vec2 screenSize;
uniform float time;

uniform float transitionEffect;
uniform float distortion;

uniform vec2 shake;

vec2 curve(vec2 uv)
{
	uv = (uv - 0.5) * 2.0;
	uv *= 1.15;
	uv.x *= 1.0 + pow((abs(uv.y) / 5.0), 2.0);
	uv.y *= 1.0 + pow((abs(uv.x) / 4.0), 2.0);
	uv  = (uv / 2.0) + 0.5;
	uv =  uv *0.92 + 0.04;
	return uv;
}

float ramp(float y, float start, float end)
{
	float inside = step(start,y) - step(end,y);
	float fact = (y-start)/(end-start)*inside;
	return (1.-fact) * inside;

}

float noise(vec2 co){
    float sample = fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453 * sin(time));
    sample *= sample;
    return sample;
}

float stripes(vec2 uv)
{
	float noi = noise(uv*vec2(0.5,1.0) + vec2(1.0,3.0));
	return ramp(mod(uv.y*max(1.25, transitionEffect*20.0) + time/10.+sin(time*0.32 + sin(time*0.363)),1.0),0.2,0.6)*noi;
}

vec4 crt()
{
    vec2 q = vec2(tex.x, 1.0 - tex.y);
    vec2 uv = curve(q + shake*0.005);
    vec3 oricol = texture2D(fbotex, q).xyz;
    vec3 col;

	float x = sin(0.3*time+uv.y*21.0)*sin(0.7*time+uv.y*29.0)*sin(0.3+0.33*time+uv.y*31.0)*0.0017;

	float globalOffset = transitionEffect * 0.2;

    float stripeOffset = stripes(uv)*max(distortion, transitionEffect*30.0);
    
    vec2 sm = -shake*0.003;
    
    col.r = texture2D(fbotex,vec2(sm.x+x+uv.x+0.001+stripeOffset*0.005,sm.y+uv.y+0.001+transitionEffect*0.65+globalOffset)).x+0.05;
    col.g = texture2D(fbotex,vec2(sm.x+x+uv.x+0.000+stripeOffset*0.01,sm.y+uv.y-0.002+transitionEffect*0.45+globalOffset)).y+0.05;
    col.b = texture2D(fbotex,vec2(sm.x+x+uv.x-0.002+stripeOffset*0.015,sm.y+uv.y+0.000+transitionEffect*0.55+globalOffset)).z+0.05;
    col.r += 0.08*texture2D(fbotex,0.75*vec2(x+0.025, -0.027+globalOffset)+vec2(uv.x+0.001,uv.y+0.001)).x;
    col.g += 0.05*texture2D(fbotex,0.75*vec2(x+-0.022, -0.02+globalOffset)+vec2(uv.x+0.000,uv.y-0.002)).y;
    col.b += 0.08*texture2D(fbotex,0.75*vec2(x+-0.02, -0.018+globalOffset)+vec2(uv.x-0.002,uv.y+0.000)).z;

    col = clamp(col*0.6+0.4*col*col*1.0,0.0,1.0);
    
    vec3 wst = col + stripes(uv)*0.05;
    col = mix(col, wst, distortion);

    float vig = (0.0 + 1.0*16.0*uv.x*uv.y*(1.0-uv.x)*(1.0-uv.y));
	col *= vec3(pow(vig,0.3));

    col *= vec3(0.95,1.05,0.95);
	col *= 2.8;

	float scans = clamp( 0.35+0.35*sin(3.5*time+uv.y*screenSize.y*1.5), 0.0, 1.0);

	float s = pow(scans,1.7);
	col = col*vec3( 0.4+0.7*s) ;

    col *= 1.0+0.01*sin(110.0*time);
	if (uv.x < 0.0 || uv.x > 1.0)
		col *= 0.0;
	if (uv.y < 0.0 || uv.y > 1.0)
		col *= 0.0;

	col*=1.0-0.65*vec3(clamp((mod(q.x, 2.0)-1.0)*2.0,0.0,1.0));
    return vec4(col, 1.0);
}

void main(void)
{
    fragColor = crt();
}

