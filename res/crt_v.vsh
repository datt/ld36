#version 140
 
uniform mat4 modelviewMatrix;
uniform mat4 projectionMatrix;

in vec2 in_vertex;
in vec4 in_color;
in vec4 in_tex;

out vec4 color;
out vec2 tex;
 
void main()
{
    gl_Position = projectionMatrix * modelviewMatrix * vec4(in_vertex, 0.0, 1.0);

    color = in_color;
    tex = vec2(in_tex.s, in_tex.t);
}
