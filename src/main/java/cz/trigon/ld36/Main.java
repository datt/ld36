package cz.trigon.ld36;

import cz.dat.gaben.api.ContentManager;
import cz.dat.gaben.api.game.GameWindowBase;
import cz.dat.gaben.api.game.GameWindowFactory;
import cz.dat.gaben.util.GabeLogger;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        GabeLogger.init(AsteroidGame.class);

        ContentManager m = new ContentManager("./res", false);
        AsteroidGame g = new AsteroidGame();

        GameWindowFactory.enableSplashscreen(true, null);
        GameWindowBase window = GameWindowFactory.createPreferredGame(1280, 720, m, g);
        window.setTitle(AsteroidGame.GAME_TITLE);

        window.start();
    }
}
