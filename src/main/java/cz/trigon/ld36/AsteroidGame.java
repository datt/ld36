package cz.trigon.ld36;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;

import cz.dat.gaben.api.Settings;
import cz.dat.gaben.api.Settings.Filters;
import cz.dat.gaben.api.data.IDataProvider;
import cz.dat.gaben.api.data.TextDataProvider;
import cz.dat.gaben.api.exception.DataSyncException;
import cz.dat.gaben.api.exception.ExceptionUtil;
import cz.dat.gaben.api.game.Game;
import cz.dat.gaben.api.game.ScreenBase;
import cz.dat.gaben.api.interfaces.IFbo;
import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.api.interfaces.IShader;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.OSUtil;
import cz.trigon.ld36.screen.About;
import cz.trigon.ld36.screen.Asteroids;
import cz.trigon.ld36.screen.Menu;

public class AsteroidGame extends Game {
    public static final String GAME_TITLE = "Asteroid Wars";
    public static final int BG_COLOR = Color.color(0.175f, 0.175f, 0.175f, 1f);

    private IRenderer r;
    private IFbo screenFbo;
    private IShader crtShader;
    
    private float lastTransitionEffect = 50000.0f;
    private float transitionEffect = 50000.0f;
    
    Random rand = new Random();
    
    private float lastShakeX = 0.0f, lastShakeY = 0.0f;
    private float shakeX = 0.0f, shakeY = 0.0f;
    private float shakeIntensity = 0.0f;

    private ScreenBase gameScreen;
    
    private long startTime;

    private TextDataProvider data;

    @Override
    public void init() {
        super.init();
        
        this.startTime = System.nanoTime();
        
        this.getApi().getFont().loadFont("menuFont", "menuFont", 128);
        this.getApi().getTexture().loadTexture("czflag", "textures/cz");

        this.getApi().getSound().addSound(0, "sounds/menuMove");
        this.getApi().getSound().addSound(1, "sounds/menuEnter");
        this.getApi().getSound().addSound(2, "sounds/menuOpen");
        this.getApi().getSound().addSound(10, "sounds/hit1");
        this.getApi().getSound().addSound(20, "sounds/shoot1");
        this.getApi().getSound().addSound(30, "sounds/shield");
        this.getApi().getSound().addSound(31, "sounds/shieldDown");
        this.getApi().getSound().addSound(32, "sounds/shieldUp");
        this.getApi().getSound().addSound(40, "sounds/roundend");

        this.getApi().getSound().addMusic(0, "sounds/menuMusic1");

        this.getApi().getSound().addMusic(10, "sounds/mainMusic1");

        this.getApi().getSound().addMusic(20, "sounds/upgradeMusic1");

        Settings s = new Settings();
        s.setMinFilter(Filters.LINEAR);
        s.setMagFilter(Filters.LINEAR);

        this.r = this.getApi().getRenderer();

        this.getApi().getFbo().createFbo("screen", this.getWidth(), this.getHeight(), 1, s);
        this.getApi().getFbo().createFbo("gameField", this.getWidth(), this.getHeight(), 1, s);
        
        this.getApi().getShader().loadShader("crt", "crt_v", "crt_f");
        this.r.shader(this.getApi().getShader().getShader("crt"));
        this.r.setupShader(this.getApi().getShader().getShader("crt"), false);
        this.r.defaultShader();

        this.getApi().getTexture().finishLoading();

        try {
            File f = new File(OSUtil.getAppDataPath(), "ld36_asteroids.config");
            if(!f.exists())
                f.createNewFile();
            this.data = new TextDataProvider(f.getAbsolutePath());

            this.data.setData("shipColorR", 1f, Float.class);
            this.data.setData("shipColorG", 1f, Float.class);
            this.data.setData("shipColorB", 1f, Float.class);
            this.data.setData("masterVolume", 0.2f, Float.class);
            this.data.setData("highLevel", 0, Integer.class);

            this.data.sync(IDataProvider.MergeType.PREFER_LOCAL);

            this.getApi().getSound().setVolume(true, this.data.getData("masterVolume", Float.class));
        } catch (java.io.IOException e) {
            throw ExceptionUtil.gameBreakingException("Problem creating data file", e, this);
        } catch (DataSyncException e) {
            throw ExceptionUtil.gameBreakingException(e, this);
        }

        this.addScreen(0, new Menu(this));
        this.addScreen(1, this.gameScreen = new Asteroids(this));
        this.addScreen(3, new About(this));
        this.openScreen(0);

        this.screenFbo = this.getApi().getFbo().getFbo("screen");
        this.crtShader = this.getApi().getShader().getShader("crt");
    }

    public TextDataProvider getData() {
        return this.data;
    }

    @Override
    public void exit() {
        try {
            this.data.sync(IDataProvider.MergeType.KEEP_MEMORY);
        } catch (DataSyncException e) {
            throw ExceptionUtil.gameBreakingException(e, this);
        }
        super.exit();
    }

    public void resetGame() {
        if(this.currentScreen == this.gameScreen)
            this.openScreen(0);

        this.screens.remove(1);
        this.addScreen(1, this.gameScreen = new Asteroids(this));

        ((Menu)this.getScreen(0)).setPlayGameState(true);
    }
    
    @Override
    public void openScreen(ScreenBase s) {
        super.openScreen(s);
        
        this.transitionEffect += 1.6f;
    }
    
    @Override
    public void onTick() {
        super.onTick();
        
        this.lastShakeX = shakeX;
        this.lastShakeY = shakeY;
        
        this.shakeX = (rand.nextFloat()-0.5f)*2;
        this.shakeY = (rand.nextFloat()-0.5f)*2;

        if (this.shakeIntensity > 5.0f) {
            this.shakeIntensity *= 0.72f;
        } else {
            this.shakeIntensity *= 0.9f;
        }
        
        this.lastTransitionEffect = this.transitionEffect;
        this.transitionEffect *= 0.6f;
    }

    @Override
    public void onRenderTick(float ptt) {
        this.r.clearColor(Color.BLACK);
        this.r.clear();
        this.r.fbo(this.screenFbo);
        this.r.defaultShader();
        this.r.clearColor(AsteroidGame.BG_COLOR);
        this.r.clear();

        super.onRenderTick(ptt);

        this.r.fbo(null);

        this.r.shader(this.crtShader);
        this.crtShader.setUniform1f("time", (float)((System.nanoTime()-this.startTime) / 1000000000d));
        this.crtShader.setUniform2f("screenSize", this.getWidth(), this.getHeight());
        
        float sx = (this.lastShakeX + (this.shakeX - this.lastShakeX)*ptt)*this.shakeIntensity;
        float sy = (this.lastShakeY + (this.shakeY - this.lastShakeY)*ptt)*this.shakeIntensity;
        this.crtShader.setUniform2f("shake", sx, sy);
        
        this.crtShader.setUniform1f("transitionEffect", this.lastTransitionEffect + (this.transitionEffect - this.lastTransitionEffect)*ptt);
        this.crtShader.setUniform1f("distortion", this.currentScreen == this.gameScreen ? 0 : 0.3f);

        this.r.bindFboTexture(this.screenFbo, 0, 0);
        this.r.shapeMode(IRenderer.ShapeMode.FILLED);
        this.r.drawFullscreenQuad();
    }
    
    public void shake(float intensity) {
        this.shakeIntensity += intensity;
    }
}
