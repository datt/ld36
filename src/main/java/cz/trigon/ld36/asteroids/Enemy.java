package cz.trigon.ld36.asteroids;

import cz.dat.gaben.util.Color;
import cz.trigon.ld36.screen.Asteroids;

public class Enemy extends GameObject {

    public Enemy(Asteroids cont) {
        super(cont);
    }

    public void kill() {
        this.shouldDie = true;
        this.game.shake(5.0f);
        this.gameContainer.spawnParticleCloud(this.bb.radius*0.5f, 10f, 50, this.bb.center, Player.yellow, 0.7f, 0, 20);
    }
    
}
