package cz.trigon.ld36.asteroids;

import java.util.Random;

import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.api.interfaces.IRenderer.ShapeMode;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.Vector2;
import cz.trigon.ld36.screen.Asteroids;

public class EnemyKamikaze extends Enemy {

    private float velX = 0;
    private float velY = 0;
    
    private Player p;
    
    public EnemyKamikaze(Asteroids cont) {
        super(cont);
        
        this.bb = new CircleBB(20, -100, -100);
        this.chooseRandomPosition();
        
        this.p = cont.getPlayer();
    }
    
    public void chooseRandomPosition() {
        Random rnd = new Random();
        switch(rnd.nextInt(4)) {
            case 0:     // Spawn from top
                this.bb.center = new Vector2(rnd.nextFloat() * this.game.getWidth(), -this.bb.radius - 10);
                this.velX = rnd.nextFloat() * (rnd.nextBoolean() ? -1 : 1);
                this.velY = rnd.nextFloat() * 10;
                break;
            case 1:     // Spawn from bottom
                this.bb.center = new Vector2(rnd.nextFloat() * this.game.getWidth(), this.game.getHeight() + this.bb.radius + 10);
                this.velX = rnd.nextFloat() * (rnd.nextBoolean() ? -1 : 1);
                this.velY = rnd.nextFloat() * -10;
                break;
            case 2:     // Spawn from left
                this.bb.center = new Vector2(-this.bb.radius - 10, rnd.nextFloat() * this.game.getHeight());
                this.velX = rnd.nextFloat() * 10;
                this.velY = rnd.nextFloat() * (rnd.nextBoolean() ? -1 : 1) * 5;
                break;
            case 3:     // Spawn from right
                this.bb.center = new Vector2(this.game.getWidth() + this.bb.radius + 10, rnd.nextFloat() * this.game.getHeight());
                this.velX = rnd.nextFloat() * -10;
                this.velY = rnd.nextFloat() * (rnd.nextBoolean() ? -1 : 1) * 5;
                break;
        }
        this.lastbb.center = new Vector2(this.bb.center.x(), this.bb.center.y());
    }
    
    @Override
    public void tick() {
        super.tick();
        
        float speed = 1.1f;
        
        if (this.p.getBb().center.x() < this.bb.center.x()) {
            this.velX -= speed;
        } else {
            this.velX += speed;
        }
        
        if (this.p.getBb().center.y() < this.bb.center.y()) {
            this.velY -= speed;
        } else {
            this.velY += speed;
        }
        
        this.velX *= 0.9f;
        this.velY *= 0.9f;
        
        this.bb.center = new Vector2(this.bb.center.x() + this.velX, this.bb.center.y() + this.velY);
    }
    
    @Override
    public void renderTick(float ptt) {
        super.renderTick(ptt);
        
        IRenderer r = this.gameContainer.getGame().getApi().getRenderer();
        
        r.color(1f, 0f, 0.18f, 1f);
        r.shapeMode(ShapeMode.OUTLINE);
        r.drawCircle(this.renderbb.center, this.bb.radius);
        r.drawSegCircle(this.renderbb.center, this.bb.radius, 5);
    }

}
