package cz.trigon.ld36.asteroids;

import cz.dat.gaben.api.data.IDataProvider;
import cz.dat.gaben.api.interfaces.IInputManager;
import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.Vector2;
import cz.trigon.ld36.AsteroidGame;
import cz.trigon.ld36.screen.Asteroids;

import java.util.Random;

public class Player extends GameObject {

    public static final int shield = Color.color(0.0f, 0.75f, 1.0f, 1.0f);
    public static final int yellow = Color.color(1.0f, 1.0f, 0.0f, 1.0f);

    public int shipColor = Color.WHITE;
    private IRenderer renderer;
    private IInputManager input;

    private float speed = 10, rotSpeed = 12;
    private float a = 40, ha = 60;

    private float friction = 0.945f;

    private float xSpeed = 0, ySpeed = 0;

    private boolean engine = false;
    private boolean engineFlash = true;
    private int shootTimer = 0;

    private boolean dying;
    private int dyingTicks;

    private int weaponType = 0;

    private int secondShotTimer = 0;

    boolean hasSpecial = true;

    private int specialSteps = 15;
    private int specialRounds = 0;
    private float specialStartAngle = 0;
    private int specialTime = 0;

    private float shieldLife = 1f;
    private int shieldRegenTime = 15 * 20;
    private int shieldLevel = 0;

    private int shieldRegenTimer = 0;
    private boolean hasShield = true;

    private int invTime = 20;

    private int invTimer = 0;

    private Random rnd = new Random();

    public Player(Asteroids cont) {
        super(cont);
        this.renderer = this.game.getApi().getRenderer();
        this.input = this.game.getApi().getInput();

        this.bb = CircleBB.makeCircleBBForIsoscelesTriangle(this.a, this.ha);
        this.updateColor();
        this.resetPlayer();
    }

    public void updateColor() {
        IDataProvider d = this.game.getData();
        this.shipColor = Color.color(d.getData("shipColorR", Float.class),
                d.getData("shipColorG", Float.class),
                d.getData("shipColorB", Float.class), 1f);
    }

    public int getSpecialRounds() {
        return this.specialRounds;
    }

    public void setSpecialRounds(int i) {
        this.specialRounds = i;
    }

    public int getSpecialSteps() {
        return this.specialSteps;
    }

    public void setSpecialSteps(int specialSteps) {
        this.specialSteps = specialSteps;
    }

    public int getShieldLevel() {
        return this.shieldLevel;
    }

    public void setShieldLevel(int level) {
        level = level % 5;
        this.shieldLevel = level;
        this.shieldRegenTime = 15 * 20 - level * 3 * 20;
    }

    public void resetPlayer() {
        this.bb.center = new Vector2(this.game.getWidth() / 2, this.game.getHeight() / 2);
        this.lastbb.center = new Vector2(this.bb.center.x(), this.bb.center.y());
        this.hasSpecial = true;
        this.hasShield = true;
        this.shieldRegenTimer = 0;
        this.invTimer = 0;
        this.shieldLife = 1f;
    }

    public boolean canUseSpecial() {
        return this.hasSpecial && this.specialRounds > 0;
    }

    private void doMovement() {
        if (!this.dying) {
            if (this.input.isKeyDown(IInputManager.Keys.W)
                    || this.input.isKeyDown(IInputManager.Keys.UP)) {
                double rotRad = this.rotation * (Math.PI / 180);

                this.xSpeed += (float) Math.sin(rotRad);
                this.ySpeed += (float) -Math.cos(rotRad);

                this.engine = true;
            }

            if (this.input.isKeyDown(IInputManager.Keys.A)
                    || this.input.isKeyDown(IInputManager.Keys.LEFT)) {
                this.rotation -= this.rotSpeed;

            }

            if (this.input.isKeyDown(IInputManager.Keys.D)
                    || this.input.isKeyDown(IInputManager.Keys.RIGHT)) {
                this.rotation += this.rotSpeed;
            }

            if (this.input.isKeyDown(IInputManager.Keys.SPACE)
                    && this.shootTimer <= 0) {
                this.shootTimer = 6;
                shootNormal();
            }

            if (this.input.isKeyDown(IInputManager.Keys.LEFT_CONTROL)
                    && this.hasSpecial) {
                this.specialTime = this.specialSteps * this.specialRounds;
                this.specialStartAngle = this.rotation;
                this.hasSpecial = false;
                shootSpecialUpdate();
            }
        }

        this.xSpeed *= this.friction;
        this.ySpeed *= this.friction;

        if (this.bb.willCollideWithBox(this.game.getWidth(),
                this.game.getHeight(), xSpeed, ySpeed)) {
            this.xSpeed = -this.xSpeed / 2;
            this.ySpeed = -this.ySpeed / 2;
        }

        if (!this.dying)
            this.bb.center = new Vector2(this.bb.center.x() + this.xSpeed,
                    this.bb.center.y() + this.ySpeed);
    }

    private void shootNormal() {
        switch (this.weaponType) {
            case 0:
                shootSingle();
                break;
            case 1:
                shootTriple();
                break;
            case 2:
                shootTripleAngled();
                break;
            case 3:
                shootTripleAngled();
                this.secondShotTimer = 2;
                break;
        }
    }

    private void shootSingle() {
        double rotRad = this.rotation * (Math.PI / 180);

        float radius = this.ha / 2;

        float startPosX = (float) Math.sin(rotRad) * radius
                + this.bb.center.x();
        float startPosY = (float) -Math.cos(rotRad) * radius
                + this.bb.center.y();

        float velX = (float) Math.sin(rotRad) * 20;
        float velY = (float) -Math.cos(rotRad) * 20;

        this.gameContainer.addEntity(new Bullet(this.gameContainer,
                startPosX, startPosY, velX, velY, this));

        this.game.getApi().getSound()
                .playSound(20, this.rnd.nextFloat());
    }

    private void shootTriple() {
        float offset = 20;

        double rotRad = this.rotation * (Math.PI / 180);

        double normalRot = (this.rotation + 90f) * (Math.PI / 180);

        float radius = this.ha / 2;

        float startPosX = (float) Math.sin(rotRad) * radius
                + this.bb.center.x();
        float startPosY = (float) -Math.cos(rotRad) * radius
                + this.bb.center.y();

        float startX1 = (float) (Math.sin(normalRot) * offset + startPosX);
        float startX2 = (float) (Math.sin(normalRot - Math.PI) * offset + startPosX);
        float startY1 = (float) (-Math.cos(normalRot) * offset + startPosY);
        float startY2 = (float) (-Math.cos(normalRot - Math.PI) * offset + startPosY);

        float velX = (float) Math.sin(rotRad) * 20;
        float velY = (float) -Math.cos(rotRad) * 20;

        this.gameContainer.addEntity(new Bullet(this.gameContainer,
                startPosX, startPosY, velX, velY, this));

        this.gameContainer.addEntity(new Bullet(this.gameContainer,
                startX1, startY1, velX, velY, this));

        this.gameContainer.addEntity(new Bullet(this.gameContainer,
                startX2, startY2, velX, velY, this));

        this.game.getApi().getSound()
                .playSound(20, this.rnd.nextFloat());
    }

    private void shootTripleAngled() {
        for (int i = -1; i < 2; i++) {
            double rotRad = (this.rotation + (i * 15f)) * (Math.PI / 180);

            float radius = this.ha / 2;

            float startPosX = (float) Math.sin(rotRad) * radius
                    + this.bb.center.x();
            float startPosY = (float) -Math.cos(rotRad) * radius
                    + this.bb.center.y();

            float velX = (float) Math.sin(rotRad) * 20;
            float velY = (float) -Math.cos(rotRad) * 20;

            this.gameContainer.addEntity(new Bullet(this.gameContainer,
                    startPosX, startPosY, velX, velY, this));
        }

        this.game.getApi().getSound()
                .playSound(20, this.rnd.nextFloat());
    }

    public void shootSpecialUpdate() {
        if (this.specialTime > 0) {

            double step = 180.0 / this.specialSteps;

            double rOff = this.specialTime * step;

            double rotRad = (this.specialStartAngle + rOff) * (Math.PI / 180);
            double shipRotRad = this.rotation * (Math.PI / 180);

            float radius = this.ha / 2;

            float startPosX = (float) Math.sin(shipRotRad) * radius
                    + this.bb.center.x();
            float startPosY = (float) -Math.cos(shipRotRad) * radius
                    + this.bb.center.y();

            float velX = (float) Math.sin(rotRad) * 20;
            float velY = (float) -Math.cos(rotRad) * 20;

            this.gameContainer.addEntity(new Bullet(this.gameContainer,
                    startPosX, startPosY, velX, velY, this));

            this.gameContainer.addEntity(new Bullet(this.gameContainer,
                    startPosX, startPosY, -velX, -velY, this));

            this.game.getApi().getSound().playSound(20, this.rnd.nextFloat());

            this.specialTime--;
        }

    }

    private void processDying() {
        if (this.dying) {
            this.dyingTicks++;

            if (this.dyingTicks < 3) {
                this.gameContainer.spawnParticleCloud(10, 25, 1500,
                        this.bb.center, Color.RED, 1.0f, 0.0f, 20);
                this.game.getApi().getSound().playSound(10, 1.2f, 0.5f);
                this.game.shake(20.0f);
            }

            if (this.dyingTicks < 8) {
                this.gameContainer.spawnParticleCloud(10, 20, 1000,
                        this.bb.center, Color.RED, 1.0f, 0.0f, 20);
            } else if (this.dyingTicks > 30) {
                this.game.getApi().getSound().stopSounds();
                this.game.getApi().getSound().playSound(10, 0.4f, 1f);
                this.game.resetGame();
            }
        }
    }

    @Override
    public void tick() {
        super.tick();
        this.lastRotation = this.rotation;

        this.engineFlash = !this.engineFlash;
        this.engine = false;
        this.shootTimer--;

        if (this.secondShotTimer > 0) {
            this.secondShotTimer--;
            if (this.secondShotTimer == 0) {
                this.shootTripleAngled();
            }
        }

        this.shootSpecialUpdate();
        this.updateShield();

        this.doMovement();

        this.gameContainer.getGameObjects().forEach(o -> {
            if (o instanceof Asteroid || o instanceof Enemy) {
                if (o.bounds(this)) {
                    this.onHit(o);
                }
            }
        });

        this.processDying();
    }

    private void onHit(GameObject a) {
        if (a.bb.radius > this.bb.radius * 0.8f || a instanceof Enemy) {
            if (this.hasShield) {
                if (a instanceof Enemy) {
                    ((Enemy)a).kill();
                }
                
                if (a instanceof Asteroid) {
                    for (Asteroid newA : ((Asteroid)a).breakIntoPieces()) {
                        this.gameContainer.addEntity(newA);
                    }
                }    
                this.game.getApi().getSound().playSound(10, rnd.nextFloat());
                this.hasShield = false;
                this.shieldRegenTimer = this.shieldRegenTime;
                this.invTimer = this.invTime;

                this.game.shake(0.5f);
                this.game.getApi().getSound().playSound(30, 1.0f, 0.45f);

            } else if (this.invTimer <= 0) {
                this.dying = true;
                this.game.getApi().getSound().stopMusic();
            }
        } else {
            if(this.hasShield) {
                this.shieldLife -= a.bb.radius / 150f;
                
                this.gameContainer.spawnParticleCloud(0, 10f, 50, a.getBb().center, Color.WHITE, 0.5f, 0, 5);
                
                if (a instanceof Asteroid) {
                    ((Asteroid)a).breakIntoPieces();
                }    

                if (this.shieldLife <= 0) {
                    this.shieldLife = 0;
                    this.hasShield = false;
                    this.shieldRegenTimer = this.shieldRegenTime;
                    this.invTimer = this.invTime;
                }
            }
        }
    }

    private void updateShield() {
        if (!this.hasShield) {
            this.shieldRegenTimer--;
            if (this.shieldRegenTimer == 0) {
                this.hasShield = true;
                this.shieldLife = 1f;
            } else if (this.invTimer > 0) {
                this.invTimer--;
            }
        }
    }

    @Override
    public void renderTick(float ptt) {
        super.renderTick(ptt);

        if (!this.dying) {
            this.renderer.enableTexture(false);
            this.renderer.shapeMode(IRenderer.ShapeMode.OUTLINE);

            float x = this.renderbb.center.x();
            float y = this.renderbb.center.y() - this.ha / 2;
            float rot = this.lastRotation + (this.rotation - this.lastRotation)
                    * ptt;

            this.renderer.color(this.shipColor);
            this.renderer.setMatrix(this.renderer.identityMatrix().rotateFrom(
                    x, this.renderbb.center.y(), rot));
            this.renderer.drawTriangle(x, y, x + a / 2, y + ha, x - a / 2, y
                    + ha);

            if (this.engine && this.engineFlash) {
                this.renderer.color(Player.yellow);
                this.renderer.drawTriangle(x, y + ha + 20, x + a / 4, y + ha, x
                        - a / 4, y + ha);
            }

            if (this.engine) {
                double rotRad = this.rotation * (Math.PI / 180);

                double normalRot = (this.rotation + 90f) * (Math.PI / 180);

                float radius = this.ha / 2;

                float startPosX = (float) Math.sin(rotRad + Math.PI) * (radius + 10)
                        + this.bb.center.x();
                float startPosY = (float) -Math.cos(rotRad + Math.PI) * (radius + 10)
                        + this.bb.center.y();

                float startX = (float) (Math.sin(normalRot + Math.PI) * this.a * (rnd.nextFloat() - 0.5f) + startPosX);
                float startY = (float) (-Math.cos(normalRot + Math.PI) * this.a * (rnd.nextFloat() - 0.5f) + startPosY);

                double angle = Math.atan2(startX - this.bb.center.x(), this.bb.center.y() - startY);

                float velX = (float) Math.sin(angle) * 8;
                float velY = (float) -Math.cos(angle) * 8;

                Particle p = new Particle(startX, startY, velX, velY, 1, rnd.nextInt(10), Player.yellow);

                this.gameContainer.getParticleEngine().addParticle(p);
            }

            if (this.hasShield) {
                this.renderer.color(1f - this.shieldLife, Color.gf(Player.shield) * this.shieldLife, Color.bf(Player.shield) * this.shieldLife);
                this.renderer.drawSegCircle(this.renderbb.center, this.renderbb.radius * 1.5f, 10);
                this.gameContainer.spawnParticleCloud(0, -2f, 1, this.renderbb.center, Player.shield, 0.5f, this.renderbb.radius * 1.5f, 2);
                this.gameContainer.spawnParticleCloud(0, 2f, 1, this.renderbb.center, Player.shield, 0.5f, this.renderbb.radius * 1.5f, 2);
            } else if (this.invTimer > 0) {
                float prog = this.invTimer / (float) this.invTime;
                this.renderer.color(1f - this.shieldLife, Color.gf(Player.shield) * this.shieldLife,
                        Color.bf(Player.shield) * this.shieldLife, prog);

                float radius = this.renderbb.radius * 1.5f - (1 - prog) * 10;
                this.renderer.drawSegCircle(this.renderbb.center, radius, 10);

                this.gameContainer.spawnParticleCloud(0, 10f, (int) ((1 - prog) * 15), this.renderbb.center, Player.shield, 0.5f, radius, 4);

                if (this.invTimer == 1) {
                    this.game.shake(0.2f);
                    this.game.getApi().getSound().playSound(31, 1.0f, 0.45f);
                    this.gameContainer.spawnParticleCloud(0, 25f, 50, this.renderbb.center, Player.shield, 0.5f, radius, 4);
                }

            } else if (this.shieldRegenTimer > 0 && this.shieldRegenTimer <= 4) {
                float prog = (4 - this.shieldRegenTimer) / 4f;
                this.renderer.color(1f - this.shieldLife, Color.gf(Player.shield) * this.shieldLife,
                        Color.bf(Player.shield) * this.shieldLife, prog);
                float radius = this.renderbb.radius * 1.5f + (1 - prog) * 5;
                this.renderer.drawSegCircle(this.renderbb.center, radius, 10);

                if (this.shieldRegenTimer == 1) {
                    this.game.getApi().getSound().playSound(32, 1.0f, 0.45f);
                    this.gameContainer.spawnParticleCloud(0, -4f, 25, this.renderbb.center, Player.shield, 0.5f, radius, 4);
                }
            }

            this.renderer.setMatrix(this.renderer.identityMatrix());
        }
    }

    public void setWeaponType(int type) {
        this.weaponType = type;
    }

    public int getWeaponType() {
        return this.weaponType;
    }

    public boolean isDying() {
        return this.dying;
    }
}
