package cz.trigon.ld36.asteroids;

import cz.dat.gaben.util.Vector2;

public class CircleBB {
    public float radius;
    public Vector2 center;

    public static CircleBB makeCircleBBForIsoscelesTriangle(float base, float height) {
        float b = (float) Math.sqrt((height * height) + ((base * base) / 4));
        float r = (b * b) / (float) Math.sqrt(4 * b * b - base * base);
        return new CircleBB(r, base / 2, height - r);
    }

    public CircleBB(CircleBB original) {
        this(original.radius, original.center.x(), original.center.y());
    }

    public CircleBB() {
        this.radius = 0;
        this.center = new Vector2();
    }

    public CircleBB(float r, float centerX, float centerY) {
        this.radius = r;
        this.center = new Vector2(centerX, centerY);
    }

    public CircleBB mix(CircleBB second, float p) {
        float deltaX0 = second.center.x() - this.center.x();
        float deltaY0 = second.center.y() - this.center.y();
        float deltaR = second.radius - this.radius;

        float x = this.center.x() + deltaX0 * p;
        float y = this.center.y() + deltaY0 * p;
        float r = this.radius + deltaR * p;

        return new CircleBB(r, x, y);
    }

    public boolean collidesWith(CircleBB other) {
        float r = other.radius + this.radius;
        float px = other.center.x() - this.center.x();
        float py = other.center.y() - this.center.y();

        return ((r * r) >= (px * px + py * py));
    }

    public boolean isOutside(int w, int h) {
        return (this.center.x() + radius < 0) || (this.center.x() - radius > w)
                || (this.center.y() - radius > h) || (this.center.y() + radius < 0);
    }

    public boolean collidesWithBox(int w, int h) {
        return (this.center.x() - radius <= 0) || (this.center.x() + radius >= w)
                || (this.center.y() + radius >= h) || (this.center.y() - radius <= 0);
    }

    public boolean willCollideWithBox(int w, int h, float xSpeed, float ySpeed) {
        return (this.center.x() + xSpeed - radius <= 0) || (this.center.x() + radius + xSpeed >= w)
                || (this.center.y() + ySpeed + radius >= h) || (this.center.y() + ySpeed - radius <= 0);
    }
}
