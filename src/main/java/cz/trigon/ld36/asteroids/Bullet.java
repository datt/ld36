package cz.trigon.ld36.asteroids;

import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.Vector2;
import cz.trigon.ld36.screen.Asteroids;

import java.util.Random;

public class Bullet extends GameObject {

    public float speed = 25;
    private IRenderer renderer;
    private Random rnd = new Random();

    private GameObject parent;
    private float velX, velY;

    public Bullet(Asteroids cont, float x, float y, float velX, float velY, GameObject parent) {
        super(cont);
        this.renderer = this.game.getApi().getRenderer();

        this.bb = new CircleBB(2, x, y);
        this.velX = velX;
        this.velY = velY;

        this.parent = parent;
        this.lastbb.center = new Vector2(x, y);
    }

    @Override
    public void tick() {
        super.tick();

        this.lastbb.center = new Vector2(this.bb.center.x(), this.bb.center.y());

        this.bb.center = new Vector2(this.bb.center.x() + this.velX, this.bb.center.y() + this.velY);

        this.gameContainer.getGameObjects().stream().filter(o -> o instanceof Asteroid).forEach(o -> {
            Asteroid a = (Asteroid) o;
            if (this.bounds(a)) {
                this.shouldDie = true;
                for (Asteroid newA : a.breakIntoPieces()) {
                    this.gameContainer.addEntity(newA);
                }

                this.gameContainer.addPoints((int)(a.bb.radius / 10f));
                this.gameContainer.checkCombo();
                this.game.getApi().getSound().playSound(10, rnd.nextFloat());
            }
        });
        
        this.gameContainer.getGameObjects().stream().filter(o -> o instanceof Enemy).forEach(o -> {
            Enemy a = (Enemy) o;
            if (this.bounds(a)) {
                this.shouldDie = true;
                
                a.kill();

                this.gameContainer.addPoints(100);
                this.gameContainer.checkCombo();
                this.game.getApi().getSound().playSound(10, rnd.nextFloat());
            }
        });
    }

    @Override
    public void renderTick(float ptt) {
        super.renderTick(ptt);

        float offsetX = this.velX * ptt;
        float offsetY = this.velY * ptt;

        this.renderer.color(Color.WHITE);
        this.renderer.drawLine(this.lastbb.center.x() + offsetX, this.lastbb.center.y() + offsetY, this.bb.center.x() + offsetX, this.bb.center.y() + offsetY);
    }

    public GameObject getParent() {
        return this.parent;
    }
}
