package cz.trigon.ld36.asteroids;

import cz.dat.gaben.api.interfaces.ITickListener;
import cz.dat.gaben.util.Matrix4;
import cz.trigon.ld36.AsteroidGame;
import cz.trigon.ld36.screen.Asteroids;

public abstract class GameObject implements ITickListener {

    CircleBB bb, renderbb, lastbb;
    float rotation, lastRotation;
    boolean shouldDie = false;

    protected AsteroidGame game;
    protected Asteroids gameContainer;

    public GameObject(Asteroids cont) {
        this.gameContainer = cont;
        this.game = (AsteroidGame) cont.getGame();

        this.bb = new CircleBB();
        this.renderbb = new CircleBB();
        this.lastbb = new CircleBB();
    }

    public CircleBB getBb() {
        return this.bb;
    }

    public float getRotation() {
        return this.rotation;
    }

    @Override
    public void tick() {
        this.lastbb = new CircleBB(this.bb);
    }

    @Override
    public void renderTick(float ptt) {
        this.renderbb = this.lastbb.mix(this.bb, ptt);
    }

    public boolean bounds(GameObject other) {
        return this.bb.collidesWith(other.bb);
    }

    public boolean shouldDie() {
        return this.shouldDie;
    }
}
