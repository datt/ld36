package cz.trigon.ld36.asteroids;

import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.api.interfaces.ITickListener;
import cz.trigon.ld36.screen.Asteroids;

public class ParticleEngine implements ITickListener {

    private Particle[] particles;

    private int maxParticles;

    private int pCount;
    private int startPosition;
    
    private Asteroids context;

    public ParticleEngine(Asteroids context) {
        this.particles = new Particle[(this.maxParticles = 32768)];
        
        this.context = context;
        
        this.pCount = 0;
        this.startPosition = 0;
    }

    public void addParticle(Particle p) {
        if(this.pCount < this.maxParticles) {
            this.pCount++;
        } else {
            this.startPosition++;
            this.startPosition %= this.maxParticles;
        }

        int putPosition = (this.startPosition + (this.pCount - 1)) % this.maxParticles;
        
        particles[putPosition] = p;
    }

    @Override
    public void tick() {
        if(this.pCount > 0) {
            int endPosition = endPosition();
            int movePosition = this.startPosition;

            for(int currentPosition = this.startPosition; currentPosition != endPosition; currentPosition++) {
                currentPosition %= this.maxParticles;
                Particle p = particles[currentPosition];
                p.tick();
                if(!p.isDead()) {
                    this.particles[movePosition] = p;
                    movePosition++;
                    movePosition %= this.maxParticles;
                } else {
                    this.pCount--;
                }
            }
        }
    }

    @Override
    public void renderTick(float ptt) {
        if(this.pCount > 0) {
            IRenderer r = this.context.getGame().getApi().getRenderer();
            r.flush();
            
            int start = this.startPosition;
            int end = endPosition();
            
            r.pointSize(2);
            
            for(int i = start; i != end; i++) {
                i %= this.maxParticles;
                Particle p = particles[i];

                float x = p.getPartialX(ptt);
                float y = p.getPartialY(ptt);
                
                r.color(p.getColor());
                r.drawPoint(x, y);
            }
            
        }
    }

    public int getCount() {
        return this.pCount;
    }

    private int endPosition() {
        if(this.pCount == this.maxParticles) {
            return (this.startPosition - 1) == -1 ? this.maxParticles
                    : (this.startPosition - 1);
        } else {
            return ((this.startPosition + this.pCount) % (this.maxParticles + 1));
        }
    }
    
}
