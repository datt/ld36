package cz.trigon.ld36.asteroids;

import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.Vector2;
import cz.trigon.ld36.screen.Asteroids;

import java.util.Random;

public class Asteroid extends GameObject {

    public Vector2[] vertices;
    private Random rnd = new Random();
    private IRenderer renderer;
    private int color = Color.WHITE;
    private float velX, velY;
    private int lifeTime;

    public Asteroid(Asteroids cont) {
        super(cont);
        this.renderer = this.game.getApi().getRenderer();
    }

    public void generateRandom() {
        this.generateRandom(5 + this.rnd.nextInt(6), 69 + this.rnd.nextFloat() * 69);
    }

    public void generateRandom(int vertCount, float radius) {
        this.vertices = new Vector2[vertCount];

        for (int i = 0; i < vertCount; i++) {
            float angle = i * 2 * (float) Math.PI / vertCount;
            float r = radius - this.rnd.nextFloat() * radius * 0.2f;
            this.vertices[i] = new Vector2(r * (float) Math.cos(angle), r * (float) Math.sin(angle));
        }

        this.bb = new CircleBB(radius * 0.9f, 0, 0);
    }

    public void chooseRandomPosition() {
        switch(this.rnd.nextInt(4)) {
            case 0:     // Spawn from top
                this.bb.center = new Vector2(rnd.nextFloat() * this.game.getWidth(), -this.bb.radius - 10);
                this.velX = rnd.nextFloat() * (rnd.nextBoolean() ? -1 : 1);
                this.velY = rnd.nextFloat() * 10;
                break;
            case 1:     // Spawn from bottom
                this.bb.center = new Vector2(rnd.nextFloat() * this.game.getWidth(), this.game.getHeight() + this.bb.radius + 10);
                this.velX = rnd.nextFloat() * (rnd.nextBoolean() ? -1 : 1);
                this.velY = rnd.nextFloat() * -10;
                break;
            case 2:     // Spawn from left
                this.bb.center = new Vector2(-this.bb.radius - 10, rnd.nextFloat() * this.game.getHeight());
                this.velX = rnd.nextFloat() * 10;
                this.velY = rnd.nextFloat() * (rnd.nextBoolean() ? -1 : 1) * 5;
                break;
            case 3:     // Spawn from right
                this.bb.center = new Vector2(this.game.getWidth() + this.bb.radius + 10, rnd.nextFloat() * this.game.getHeight());
                this.velX = rnd.nextFloat() * -10;
                this.velY = rnd.nextFloat() * (rnd.nextBoolean() ? -1 : 1) * 5;
                break;
        }
        this.lastbb.center = new Vector2(this.bb.center.x(), this.bb.center.y());
    }

    public Asteroid[] breakIntoPieces() {
        this.shouldDie = true;
        this.game.shake(this.bb.radius*0.05f);

        if(this.bb.radius < 15)
            return new Asteroid[0];

        int pieces = 1 + this.rnd.nextInt(4);
        Asteroid[] ret = new Asteroid[pieces];

        for(int i = 0; i < pieces; i++) {
            Asteroid a = new Asteroid(this.gameContainer);
            a.generateRandom((int)(this.vertices.length * 0.75), this.bb.radius * 0.4f);
            a.bb.center = new Vector2(this.bb.center);
            a.lastbb.center = new Vector2(this.bb.center);
            a.velX = rnd.nextFloat() * 10 * (rnd.nextBoolean() ? -1 : 1);
            a.velY = rnd.nextFloat() * 10 * (rnd.nextBoolean() ? -1 : 1);
            ret[i] = a;
        }

        this.gameContainer.spawnParticleCloud(this.bb.radius, 20, (int) this.bb.radius * 4, this.bb.center,
                0x444444FF, 0.5f + 0.5f * this.rnd.nextFloat(), 0.0f, 20);

        return ret;
    }

    @Override
    public void tick() {
        super.tick();
        this.lastRotation = this.rotation;

        if(this.bounds(this.gameContainer.getPlayer())) {
            this.color = Color.RED;
        } else {
            this.color = Color.WHITE;
        }

        if(this.lifeTime < 80) {
            this.lifeTime++;
        } else {
            if(this.bb.isOutside(this.game.getWidth(), this.game.getHeight()))
                this.shouldDie = true;
        }

        this.rotation += 1;
        this.bb.center = new Vector2(this.bb.center.x() + this.velX, this.bb.center.y() + this.velY);
    }

    @Override
    public void renderTick(float ptt) {
        super.renderTick(ptt);

        if (this.vertices != null && this.vertices.length > 0) {
            this.renderer.color(this.color);
            float rot = this.lastRotation + (this.rotation - this.lastRotation) * ptt;
            this.renderer.primitiveMode(IRenderer.PrimitiveMode.LINE_STRIP);
            this.renderer.setMatrix(this.renderer.identityMatrix().translate(this.renderbb.center.x(), this.renderbb.center.y()).rotateFrom(this.renderbb.center.x(),
                    this.renderbb.center.y(), rot));

            this.renderer.shapeMode(IRenderer.ShapeMode.OUTLINE);
            for(int i = 0; i < this.vertices.length; i++) {
                this.renderer.vertex(this.vertices[i]);
            }
            this.renderer.vertex(this.vertices[0]);

            this.renderer.flush();
            this.renderer.setMatrix(this.renderer.identityMatrix());
        }
    }
}
