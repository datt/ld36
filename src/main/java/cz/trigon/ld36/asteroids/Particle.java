package cz.trigon.ld36.asteroids;

import cz.dat.gaben.api.interfaces.ITickListener;
import cz.dat.gaben.util.Color;

public class Particle implements ITickListener {
    
    private float lastPosX;
    private float lastPosY;
    
    private float friction;
    
    private float posX;
    private float posY;
    
    private float velX;
    private float velY;
    
    private int color;
    
    private int lifetime;
    private boolean dead;
    
    public Particle(float x, float y, float velX, float velY, float friction, int lifetime, int color) {
        this.posX = x;
        this.posY = y;
        
        this.lastPosX = x;
        this.lastPosY = y;
        
        this.velX = velX;
        this.velY = velY;
        this.color = color;
        this.friction = friction;
        this.lifetime = lifetime;
        this.dead = false;
    }

    public float getPosX() {
        return this.posX;
    }

    public float getPosY() {
        return this.posY;
    }

    public int getColor() {
        return this.color;
    }
    
    public float getPartialX(float ptt) {
        return this.lastPosX + (this.posX - this.lastPosX) * ptt;
    }
    
    public float getPartialY(float ptt) {
        return this.lastPosY + (this.posY - this.lastPosY) * ptt;
    }
    
    public boolean isDead() {
        return this.dead;
    }

    @Override
    public void tick() {
        this.lastPosX = this.posX;
        this.lastPosY = this.posY;
        
        this.posX += this.velX;
        this.posY += this.velY;
        
        this.velX *= this.friction;
        this.velY *= this.friction;
        
        this.lifetime--;
        
        if (this.lifetime <= 0) {
            this.dead = true;
        }
    }

    @Override
    public void renderTick(float ptt) {

    }    
    
}
