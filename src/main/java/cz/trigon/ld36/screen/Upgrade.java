package cz.trigon.ld36.screen;

import cz.dat.gaben.api.game.ScreenBase;
import cz.dat.gaben.api.interfaces.IFont;
import cz.dat.gaben.api.interfaces.IFontRenderer;
import cz.dat.gaben.api.interfaces.IInputManager;
import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.util.Anchor;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.Vector2;
import cz.trigon.ld36.asteroids.CircleBB;
import cz.trigon.ld36.asteroids.Player;

import java.util.function.IntSupplier;

public class Upgrade extends ScreenBase {

    private Asteroids gameScreen;
    private IRenderer r;
    private IFontRenderer fontR;
    private IFont font;

    private CircleBB previewShipBB;
    private int psBase = 200, psHeight = 300;
    private float rotation, lastRotation;

    private UpdateItem[] items;
    private int selectedItem;

    private class UpdateItem {
        public String label;
        public int maxLevel;
        public IntSupplier currentLevel;
        public IntSupplier nextLevelCost;
        public Runnable buyAction;
        public int currentLevelOffset;

        public UpdateItem(String label, int maxLevel, IntSupplier clvl, IntSupplier nextLevelCost, Runnable buy) {
            this.label = label;
            this.maxLevel = maxLevel;
            this.nextLevelCost = nextLevelCost;
            this.currentLevel = clvl;
            this.buyAction = buy;
        }

        public int getLevel() {
            return this.currentLevel.getAsInt() + this.currentLevelOffset;
        }

        public boolean isMax() {
            return this.getLevel() == this.maxLevel;
        }
    }

    public Upgrade(Asteroids gameScreen) {
        super(gameScreen.getGame(), "Upgrade ship");
        this.gameScreen = gameScreen;

        this.r = game.getApi().getRenderer();
        this.fontR = game.getApi().getFontRenderer();
        this.font = game.getApi().getFont().getFont("menuFont");

        this.previewShipBB = CircleBB.makeCircleBBForIsoscelesTriangle(this.psBase, this.psHeight);
        this.previewShipBB.center = new Vector2(this.psBase * 1.3f, game.getHeight() / 2);

        this.items = new UpdateItem[]{
                new UpdateItem("Weapon: ", 4, () -> gameScreen.getPlayer().getWeaponType(),
                        () -> (int) Math.pow(gameScreen.getPlayer().getWeaponType() + 1, 4) * 100,
                        () -> gameScreen.getPlayer().setWeaponType(gameScreen.getPlayer().getWeaponType() + 1)),
                new UpdateItem("Special: ", 4, () -> gameScreen.getPlayer().getSpecialRounds(),
                        () -> (int) Math.pow(gameScreen.getPlayer().getSpecialRounds() + 1, 2) * 400,
                        () -> gameScreen.getPlayer().setSpecialRounds(gameScreen.getPlayer().getSpecialRounds() + 1)),

                new UpdateItem("Shields: ", 5, () -> gameScreen.getPlayer().getShieldLevel(),
                        () -> (int)(((gameScreen.getPlayer().getShieldLevel() + 1)/(gameScreen.getPlayer().getShieldLevel() + 3f)) * 2500f), () -> {
                            gameScreen.getPlayer().setShieldLevel(gameScreen.getPlayer().getShieldLevel() + 1);
                })
        };

        this.items[0].currentLevelOffset = 1;
        this.items[2].currentLevelOffset = 1;
    }

    @Override
    public void tick() {
        this.lastRotation = this.rotation;
        this.rotation += 1;
    }

    @Override
    public void renderTick(float ptt) {
        this.r.fbo(this.game.getApi().getFbo().getFbo("screen"));
        this.r.defaultShader();

        float rot = this.lastRotation + (this.rotation - this.lastRotation) * ptt;
        float x = this.previewShipBB.center.x();
        float y = this.previewShipBB.center.y() - this.psHeight / 2;

        this.r.setMatrix(this.r.identityMatrix().rotateFrom(
                this.previewShipBB.center.x(), this.previewShipBB.center.y(), rot));
        this.r.enableTexture(false);

        this.r.color(Color.RED);
        this.r.lineWidth(3);
        for(int i = 0; i < this.items[1].getLevel(); i++) {
            float bo = i * (this.psBase / 8f);
            float nx = x - this.psBase / 2 + bo - 1;
            float ny = (y + this.psHeight) - this.psHeight * (bo * 2 / this.psBase) - 5;
            this.r.drawLine(nx, ny, nx, ny - 30);
            this.r.drawLine(nx + (this.psBase - bo * 2), ny, nx + (this.psBase - bo * 2), ny - 30);
        }
        this.r.flush();
        this.r.lineWidth(1);

        this.r.color(this.gameScreen.getPlayer().shipColor);
        this.r.shapeMode(IRenderer.ShapeMode.OUTLINE);
        this.r.drawTriangle(x, y, x + this.psBase / 2, y + this.psHeight, x - this.psBase / 2, y + this.psHeight);

        float weaponLinesWidth = this.psBase / this.items[0].getLevel() + 1;
        float wx = x - this.psBase / 2;
        for (int i = 0; i < this.items[0].getLevel(); i++) {
            this.r.drawLine(wx, y + this.psHeight, x, y);
            wx += weaponLinesWidth;
        }
        
        int shieldlvl = this.items[2].getLevel();
        
        for (int i = 0; i < shieldlvl; i++) {
            float rott = rot * (2 + i * 0.5f) + 262.6f * i;
            
            this.r.color(Player.shield);
            
            this.r.setMatrix(this.r.identityMatrix().rotateFrom(
                    this.previewShipBB.center.x(), this.previewShipBB.center.y(), rott));
            
            this.r.drawSegCircle(this.previewShipBB.center, this.previewShipBB.radius * 1.35f, 10+i);
            
        }
        this.r.setMatrix(this.r.identityMatrix());

        x += this.previewShipBB.radius * 1.5f;

        this.r.enableTexture(true);
        this.r.color(Color.WHITE);
        this.fontR.setFont(this.font);
        this.fontR.setSize(32);
        this.fontR.setAnchor(Anchor.TOP_LEFT);
        this.fontR.drawString("YOU HAVE " + this.gameScreen.getPoints(), x, y + this.previewShipBB.radius * 2);
        this.fontR.drawString(">> PRESS ESC TO CONTINUE <<", x,
                y + this.previewShipBB.radius * 2 + this.fontR.getMaxHeight());
        this.fontR.drawString(">> PRESS ENTER TO BUY <<",
                x, y + this.previewShipBB.radius * 2 + this.fontR.getMaxHeight() * 2);

        this.fontR.drawString("COST: " + (this.items[this.selectedItem].isMax() ? "MAX" :
                        this.items[this.selectedItem].nextLevelCost.getAsInt() + ""),
                x, y - this.fontR.getMaxHeight() * 2);

        this.fontR.setSize(40);
        y -= this.fontR.getMaxHeight() / 2;

        for (int i = 0; i < this.items.length; i++) {
            if (i == this.selectedItem)
                this.r.color(Color.WHITE);
            else
                this.r.color(Color.BLACK);

            float ww = 270;
            this.fontR.drawString(this.items[i].label, x,
                    y + (this.fontR.getMaxHeight() + 15) * i);

            this.renderUpgradeProgress(this.items[i].getLevel(),
                    this.items[i].maxLevel, x + ww,
                    y + (this.fontR.getMaxHeight() + 15) * i + 15);
        }
    }

    private void buy() {
        int cost = this.items[this.selectedItem].nextLevelCost.getAsInt();
        if (cost <= this.gameScreen.getPoints() && !this.items[this.selectedItem].isMax()) {
            this.gameScreen.removePoints(cost);
            this.items[this.selectedItem].buyAction.run();
        }
    }

    private void renderUpgradeProgress(int level, int maxLevel, float x, float y) {
        this.r.enableTexture(false);
        int maxWidth = 400;
        int height = 30;

        float oneBoxWidth = maxWidth / (float) maxLevel;
        for (int i = 0; i < maxLevel; i++) {
            this.r.shapeMode(i < level ? IRenderer.ShapeMode.FILLED : IRenderer.ShapeMode.OUTLINE);
            this.r.drawRect(x + 3, y, x + oneBoxWidth - 3, y + height);
            x += oneBoxWidth;
        }
    }

    @Override
    public void onKeyDown(int key) {
        super.onKeyDown(key);
        if (key == IInputManager.Keys.ENTER) {
            this.buy();
        }

        if (key == IInputManager.Keys.ESCAPE) {
            this.game.openScreen(this.gameScreen);
        }

        if (key == IInputManager.Keys.DOWN) {
            this.selectedItem = (this.selectedItem + 1) % this.items.length;
        }

        if (key == IInputManager.Keys.UP) {
            this.selectedItem--;
            if (this.selectedItem == -1)
                this.selectedItem = this.items.length - 1;
        }
    }

    @Override
    public void onOpening() {
        super.onOpening();
        this.game.getApi().getSound().playMusic(20, 0.8f, true);
    }

    @Override
    public void onClosing() {
        super.onClosing();
        this.game.getApi().getSound().stopMusic();
    }
}
