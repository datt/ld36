package cz.trigon.ld36.screen;

import cz.dat.gaben.api.data.IDataProvider;
import cz.dat.gaben.api.game.ScreenBase;
import cz.dat.gaben.api.interfaces.*;
import cz.dat.gaben.util.Anchor;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.Vector2;
import cz.trigon.ld36.AsteroidGame;
import cz.trigon.ld36.asteroids.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Asteroids extends ScreenBase {

    private IRenderer r;
    private IFontRenderer fontR;
    private IFont font;

    private Player p;
    private List<GameObject> gameObjects, toAdd, toRemove;

    private AsteroidGame ag;
    private IFbo gameFieldFbo, screenFbo;

    private float roundPercentage = 1f;
    private Random rnd = new Random();

    private ParticleEngine particleEngine;
    private int points = 0, combo = 1;

    private float roundTimeSec = 10f;
    private final float maxRoundTimeSec = 60f;
    private boolean resetNextRound;
    private int level = 1;

    private int endingTicks = 0;

    public Asteroids(AsteroidGame game) {
        super(game, "Asteroids");

        this.p = new Player(this);

        this.particleEngine = new ParticleEngine(this);

        this.gameObjects = new LinkedList<>();
        this.toAdd = new LinkedList<>();
        this.toRemove = new LinkedList<>();

        this.r = game.getApi().getRenderer();
        this.fontR = game.getApi().getFontRenderer();
        this.gameFieldFbo = game.getApi().getFbo().getFbo("gameField");
        this.screenFbo = game.getApi().getFbo().getFbo("screen");
        // TODO: change font
        this.font = game.getApi().getFont().getFont("menuFont");

        this.spawnAsteroids(8);
    }

    @Override
    public void onKeyDown(int key) {
        super.onKeyDown(key);
        if (key == IInputManager.Keys.ESCAPE) {
            if (this.p.isDying()) {
                ((AsteroidGame) this.game).resetGame();
            }
            this.game.openScreen(0);
            this.game.getApi().getSound().stopMusic();
        }
    }

    @Override
    public void tick() {
        this.p.tick();
        this.gameObjects.forEach(o -> {
            o.tick();
            if (o.shouldDie()) {
                this.toRemove.add(o);
            }
        });

        this.toAdd.forEach(a -> this.gameObjects.add(a));
        this.toAdd.clear();

        this.toRemove.forEach(r -> this.gameObjects.remove(r));
        this.toRemove.clear();

        if (this.endingTicks == 0) {
            if (this.game.getWindow().getPassedTicks() % (int) (60 * (0.55f + 0.45f * 1f / this.level)) == 0) {
                this.spawnAsteroids(0);
            }

            if (this.level > 1 && this.game.getWindow().getPassedTicks() % (int) (60 * (0.8f + 0.2f * 1f / this.level)) == 0 ) {
                this.spawnEnemies(0);
            }

            this.roundPercentage -= 1 / (20f * this.roundTimeSec);

            if (this.roundPercentage <= 0) {
                this.endingTicks = 1;
                ((AsteroidGame) this.game).shake(5f);
                this.game.getApi().getSound().stopMusic();
                this.game.getApi().getSound().playSound(40, 1.0f, 0.65f);
                this.gameObjects.stream().filter(o -> o instanceof Asteroid).forEach(a -> {
                    this.spawnParticleCloud(a.getBb().radius, 40, (int) a.getBb().radius * 25,
                            a.getBb().center, Color.WHITE, 0f, 0.0f, 20);
                    this.toRemove.add(a);
                });

                this.gameObjects.stream().filter(o -> o instanceof Enemy).forEach(e -> this.toRemove.add(e));
            }
        } else {
            if (this.endingTicks >= 40) {
                this.stopRound();
            }

            this.endingTicks++;
        }

        this.particleEngine.tick();
    }

    private void stopRound() {
        if (!this.p.isDying()) {
            this.game.openScreen(new Upgrade(this));
            this.resetNextRound = true;
        }

        IDataProvider d = ((AsteroidGame) this.game).getData();
        if (this.level > d.getData("highLevel", Integer.class))
            d.setData("highLevel", this.level, Integer.class);
    }

    public List<Integer> spawnedDebug = new ArrayList<>();

    private void spawnAsteroids(int c) {
        c = (c == 0 ? this.rnd.nextInt(Math.min(5 + this.level, 9)) : c);
        if (this.spawnedDebug.size() < this.level)
            this.spawnedDebug.add(this.level - 1, 0);

        this.spawnedDebug.set(this.level - 1, this.spawnedDebug.get(this.level - 1) + c);
        for (int i = 0; i < c; i++) {
            Asteroid a = new Asteroid(this);
            a.generateRandom();
            a.chooseRandomPosition();
            this.gameObjects.add(a);
        }
    }

    private void spawnEnemies(int c) {
        c = (c == 0 ? this.rnd.nextInt(2) : c);

        for (int i = 0; i < c; i++) {
            EnemyKamikaze e = new EnemyKamikaze(this);
            this.gameObjects.add(e);
        }
    }

    public List<GameObject> getGameObjects() {
        return this.gameObjects;
    }

    @Override
    public void renderTick(float ptt) {
        this.r.fbo(this.gameFieldFbo);
        this.r.defaultShader();
        this.r.clearColor(AsteroidGame.BG_COLOR);
        this.r.clear();
        this.r.color(0x66D4BC00);
        this.r.enableTexture(false);
        for (int x = 0; x <= this.gameFieldFbo.getWidth(); x += 40) {
            this.r.drawLine(x == 0 ? 1 : x, 0, x == 0 ? 1 : x, this.gameFieldFbo.getHeight());
        }

        for (int y = 0; y <= this.gameFieldFbo.getHeight(); y += 40) {
            this.r.drawLine(0, y == this.gameFieldFbo.getHeight() ? y - 1 : y, this.gameFieldFbo.getWidth(),
                    y == this.gameFieldFbo.getHeight() ? y - 1 : y);
        }

        this.particleEngine.renderTick(ptt);
        this.gameObjects.forEach(o -> o.renderTick(ptt));
        this.p.renderTick(ptt);
        this.r.fbo(this.screenFbo);
        this.r.passthroughShader();
        this.r.bindFboTexture(this.gameFieldFbo, 0, 0);
        this.r.shapeMode(IRenderer.ShapeMode.FILLED);
        this.r.drawRect(40, 40, this.screenFbo.getWidth() - 40, this.screenFbo.getHeight() - 40);
        this.r.defaultShader();

        this.drawBar();
        this.fontR.setAnchor(Anchor.CENTER_LEFT);
        this.fontR.setFont(this.font);
        this.fontR.setSize(36);
        this.fontR.drawString(this.points + "", 80, 75);
        this.fontR.setAnchor(Anchor.CENTER_CENTER);
        this.fontR.drawString("LVL " + this.level, this.screenFbo.getWidth() / 2, 75);
        if (this.combo > 1) {
            this.fontR.setAnchor(Anchor.CENTER_RIGHT);
            this.fontR.drawString("COMBO " + this.combo, this.screenFbo.getWidth() - 80, 75);
        }
        if (this.p.canUseSpecial()) {
            this.fontR.setAnchor(Anchor.CENTER_LEFT);
            this.r.color(Color.RED);
            this.fontR.drawString("S", 80, this.screenFbo.getHeight() - 75);
        }
    }

    private void drawBar() {
        this.r.color(Color.WHITE);
        this.r.enableTexture(false);
        this.r.shapeMode(IRenderer.ShapeMode.FILLED);

        this.r.drawRect(35 + (this.game.getWidth() / 2 - 35) * (1f - roundPercentage), this.game.getHeight() - 45,
                this.game.getWidth() / 2, this.game.getHeight() - 30);
        this.r.drawRect(this.game.getWidth() / 2, this.game.getHeight() - 45, this.game.getWidth() - 35 -
                (this.game.getWidth() / 2 - 35) * (1f - roundPercentage), this.game.getHeight() - 30);
    }

    public void addEntity(GameObject o) {
        this.toAdd.add(o);
    }

    public Player getPlayer() {
        return this.p;
    }

    public ParticleEngine getParticleEngine() {
        return this.particleEngine;
    }

    public void spawnParticleCloud(float radius, float vel, int count, Vector2 center, int color, float colorFuzz, float minRad, int lifetime) {
        for (int i = 0; i < count; i++) {
            float velocity = this.rnd.nextFloat() * vel;

            double dir = rnd.nextDouble() * Math.PI * 2;
            float velX = (float) (Math.sin(dir)) * velocity;
            float velY = (float) (-Math.cos(dir)) * velocity;

            float offset = minRad + this.rnd.nextFloat() * radius * 0.3f;

            float startOffsetX = ((float) (Math.sin(dir))) * offset;
            float startOffsetY = ((float) (-Math.cos(dir))) * offset;

            float colorOffset = 1.0f - colorFuzz * rnd.nextFloat();

            int c = Color.color(colorOffset * Color.rf(color), colorOffset * Color.gf(color), colorOffset * Color.bf(color), 1f);

            Particle p = new Particle(center.x() + startOffsetX, center.y() + startOffsetY, velX, velY, 1.0f, lifetime + this.rnd.nextInt(lifetime), c);
            this.particleEngine.addParticle(p);
        }
    }

    public void addPoints(int points) {
        this.points += points * (this.combo > 2 ? (this.combo / (2)) : 1);
    }

    public int getPoints() {
        return this.points;
    }

    public void removePoints(int points) {
        this.points -= points;
    }

    @Override
    public void onOpening() {
        super.onOpening();
        this.game.getApi().getSound().playMusic(10, 0.75f, true);

        if (this.resetNextRound) {
            this.roundPercentage = 1f;
            this.gameObjects.clear();
            this.particleEngine = new ParticleEngine(this);
            this.spawnAsteroids(10);
            this.p.resetPlayer();
            this.combo = 1;
            this.level++;
            this.roundTimeSec = Math.min(this.roundTimeSec + 2.5f, this.maxRoundTimeSec);
            this.endingTicks = 0;
        }
    }

    @Override
    public void onClosing() {
        super.onClosing();
        this.game.getApi().getSound().stopMusic();
    }

    private int lastComboTime = 0;

    public void checkCombo() {
        int pt = this.game.getWindow().getPassedTicks();
        int passedTime = pt - this.lastComboTime;
        if (passedTime <= 15 && passedTime > 5) {
            this.combo++;
        } else {
            if (passedTime > 20) {
                this.combo = 1;
            }
        }

        this.lastComboTime = pt;
    }
}
