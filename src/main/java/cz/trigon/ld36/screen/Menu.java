package cz.trigon.ld36.screen;

import cz.dat.gaben.api.data.IDataProvider;
import cz.dat.gaben.api.exception.ExceptionHandling;
import cz.dat.gaben.api.exception.ExceptionUtil;
import cz.dat.gaben.api.game.Game;
import cz.dat.gaben.api.game.ScreenBase;
import cz.dat.gaben.api.interfaces.IFbo;
import cz.dat.gaben.api.interfaces.IFont;
import cz.dat.gaben.api.interfaces.IFontRenderer;
import cz.dat.gaben.api.interfaces.IInputManager;
import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.api.interfaces.IShader;
import cz.dat.gaben.api.interfaces.IRenderer.ShapeMode;
import cz.dat.gaben.util.Anchor;
import cz.dat.gaben.util.Color;
import cz.trigon.ld36.AsteroidGame;

import java.util.List;
import java.util.Random;

public class Menu extends ScreenBase {

    private class MenuItem {
        public String text;
        public boolean enabled;
        public Runnable onPress;

        public MenuItem(String text, Runnable onPress) {
            this.text = text;
            this.onPress = onPress;
            this.enabled = true;
        }
    }

    private IRenderer r;
    private IFontRenderer fontRenderer;
    private IFont font;
    private MenuItem[] items;
    private int selectedItem;
    private Random rnd = new Random();
    private int aCol = Color.WHITE;

    public Menu(AsteroidGame game) {
        super(game, "Menu");

        this.r = game.getApi().getRenderer();
        this.fontRenderer = game.getApi().getFontRenderer();
        this.font = game.getApi().getFont().getFont("menuFont");

        this.r = game.getApi().getRenderer();

        this.items = new MenuItem[4];
        this.items[0] = new MenuItem("Play game", () -> game.openScreen(1));
        this.items[1] = new MenuItem("Reset game", () -> {
            game.resetGame();
            this.setPlayGameState(false);
            game.openScreen(1);
        });
        this.items[2] = new MenuItem("About", () -> game.openScreen(3));
        this.items[3] = new MenuItem("Exit", game::exit);

        this.items[1].enabled = false;
    }

    public void setPlayGameState(boolean play) {
        if (play) {
            this.items[0].text = "Play game";
            this.items[1].enabled = false;
        } else {
            this.items[0].text = "Continue game";
            this.items[1].enabled = true;
        }
    }

    @Override
    public void tick() {

    }

    @Override
    public void renderTick(float ptt) {
        this.fontRenderer.setFont(this.font);
        this.fontRenderer.setAnchor(Anchor.CENTER_CENTER);
        this.fontRenderer.setSize(64);
        this.fontRenderer.drawString(AsteroidGame.GAME_TITLE, this.game.getWidth() / 2,
                this.fontRenderer.getMaxHeight() + 20);

        float y = this.fontRenderer.getMaxHeight() * 2 + 60;
        this.fontRenderer.setSize(48);
        for (int i = 0; i < this.items.length; i++) {
            if (this.items[i].enabled) {
                this.r.color(Color.WHITE);
            } else {
                this.r.color(Color.BLACK);
            }
            this.fontRenderer.drawString(this.items[i].text, this.game.getWidth() / 2, y);

            if (i == this.selectedItem) {
                this.r.color(Color.WHITE);
                this.r.enableTexture(false);
                this.r.shapeMode(ShapeMode.FILLED);
                this.r.drawTriangle(200, y - 10, 220, y, 200, y + 10);
                this.r.drawTriangle(this.game.getWidth() - 200, y - 10, this.game.getWidth() - 200, y + 10,
                         this.game.getWidth() - 220, y);
            }

            y += this.fontRenderer.getMaxHeight() + 10;
        }

        this.fontRenderer.setSize(24);
        this.r.color(this.aCol);
        this.fontRenderer.drawString("Use scroll wheel to change volume. Press R for awesomeness.",
                this.game.getWidth() / 2, y + this.fontRenderer.getMaxHeight());
        this.fontRenderer.drawString("Controls: WASD or arrow keys, Space bar and left Control",
                this.game.getWidth() / 2, y + this.fontRenderer.getMaxHeight() * 2);
        int hl = ((AsteroidGame)this.game).getData().getData("highLevel", Integer.class);
        if(hl > 0) {
            this.fontRenderer.drawString("Your greatest life achievement is level " + hl, this.game.getWidth() / 2,
                    y + this.fontRenderer.getMaxHeight() * 3);
        }

        this.r.enableTexture(false);
        this.r.color(Color.WHITE);
        this.r.shapeMode(ShapeMode.FILLED);
        this.r.drawRect(30, 30, 45, 30 + (this.game.getHeight() - 60) *
                ((AsteroidGame)this.game).getData().getData("masterVolume", Float.class));
    }

    @Override
    public void onOpening() {
        if (this.wasOpened())
            this.game.getApi().getSound().playSound(2, 1f, 0.5f);

        super.onOpening();
        this.game.getApi().getSound().playMusic(0, 1f, true);

        List<Integer> d = ((Asteroids)this.game.getScreen(1)).spawnedDebug;
        if(d != null)
            for(int i = 0; i < d.size(); i++)
                System.out.println("L" + (i + 1) + ": " + d.get(i));
    }

    @Override
    public void onClosing() {
        super.onClosing();
        this.game.getApi().getSound().playSound(1, 1f, 0.5f);
        this.game.getApi().getSound().stopMusic();
    }

    @Override
    public void onMouseScroll(float x, float y) {
        super.onMouseScroll(x, y);

        IDataProvider p = ((AsteroidGame)this.game).getData();
        float vol = p.getData("masterVolume", Float.class);
        vol += 0.05 * y;
        if(vol > 1f) vol = 1f;
        if(vol < 0) vol = 0;
        p.setData("masterVolume", vol, Float.class);
        this.game.getApi().getSound().setVolume(true, vol);
    }

    @Override
    public void onKeyDown(int key) {
        super.onKeyDown(key);

        if (key == IInputManager.Keys.W || key == IInputManager.Keys.UP) {
            this.selectedItem--;
            if (this.selectedItem == -1)
                this.selectedItem = this.items.length - 1;

            this.game.getApi().getSound().playSound(0, 1f, 0.5f);
        }

        if (key == IInputManager.Keys.S || key == IInputManager.Keys.DOWN) {
            this.selectedItem = (this.selectedItem + 1) % this.items.length;

            this.game.getApi().getSound().playSound(0, 1f, 0.5f);
        }

        if (key == IInputManager.Keys.ENTER) {
            if (this.selectedItem == 0 && !this.game.getScreen(1).wasOpened()) {
                this.setPlayGameState(false);
            }

            if (this.items[this.selectedItem].enabled) {
                this.items[this.selectedItem].onPress.run();
            }
        }

        if(key == IInputManager.Keys.R) {
            IDataProvider p = ((AsteroidGame)this.game).getData();
            float r, g, b;
            p.setData("shipColorR", r = 0.4f + (this.rnd.nextFloat() * 0.6f), Float.class);
            p.setData("shipColorG", g = 0.4f + (this.rnd.nextFloat() * 0.6f), Float.class);
            p.setData("shipColorB", b = 0.4f + (this.rnd.nextFloat() * 0.6f), Float.class);
            this.aCol = Color.color(r, g, b, 1f);
            ((Asteroids)this.game.getScreen(1)).getPlayer().updateColor();
        }
    }
}
