package cz.trigon.ld36.screen;

import cz.dat.gaben.api.game.Game;
import cz.dat.gaben.api.game.ScreenBase;
import cz.dat.gaben.api.interfaces.IFont;
import cz.dat.gaben.api.interfaces.IFontRenderer;
import cz.dat.gaben.api.interfaces.IInputManager;
import cz.dat.gaben.api.interfaces.ITexture;
import cz.dat.gaben.util.Anchor;
import cz.dat.gaben.util.Color;

public class About extends ScreenBase {
    private String[] lines = {
        "This game was made for Ludum Dare 36 Jam.",
        "The theme was \"Ancient technology\",",
        "which we may or may not have fully achieved,",
        "but we still like it.",
        "",
        "bitbucket.org/datt/ld36",
        "Made in the Czech Republic"};

    private IFont font;
    private IFontRenderer fontRenderer;
    private ITexture flag;

    public About(Game game) {
        super(game, "About");
        this.font = game.getApi().getFont().getFont("menuFont");
        this.fontRenderer = game.getApi().getFontRenderer();
        this.flag = game.getApi().getTexture().getTexture("czflag");
    }

    @Override
    public void tick() {
    }

    @Override
    public void renderTick(float ptt) {
        this.fontRenderer.setFont(this.font);
        this.fontRenderer.setAnchor(Anchor.TOP_CENTER);
        this.fontRenderer.setSize(36);

        float y = (this.game.getHeight() / 2) - (((this.fontRenderer.getMaxHeight() + 10) * this.lines.length + 100) / 2);
        this.game.getApi().getRenderer().color(Color.WHITE);
        for(int i = 0; i < this.lines.length; i++) {
            this.fontRenderer.drawString(this.lines[i], this.game.getWidth() / 2, y);
            y += this.fontRenderer.getMaxHeight() + 10;
        }

        this.game.getApi().getRenderer().drawTexture(this.flag, this.game.getWidth() / 2 - 60, y + 20);
    }

    @Override
    public void onKeyDown(int key) {
        super.onKeyDown(key);
        if(key == IInputManager.Keys.ESCAPE || key == IInputManager.Keys.ENTER) {
            this.game.openScreen(0);
        }
    }
}
